import React from 'react';
import ReactDOM from 'react-dom'
import "./index.css";

class Hello extends React.Component {
		render(){
			return(
				<h1>Hello</h1>
			)
		}
}

class World extends React.Component {
	render(){
		return(
			<p>World</p>
		)
	}
}

class HelloWorld extends React.Component {
	constructor(){
		super();
		this.name = "Aditya Sid"
	}
	render(){
		return(
			<div>
				<section className="foo">
						Hello
						<Hello/>
						<World/>
				</section>

				<section>
						<Hello/>
				</section>
				<input type="text" />
				<section>
					{this.name}
				</section>
			</div>
		)
	}
}

ReactDOM.render(<HelloWorld/>, document.getElementById('root'))
